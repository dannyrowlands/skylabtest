#!/bin/sh

if grep /var/www/vhosts/skylabtest/production/htdocs/ "/home/vagrant/.bashrc"; then
   echo "Home Dir Set"
else
   echo 'cd /var/www/vhosts/skylabtest/production/htdocs/' >> /home/vagrant/.bashrc
   echo "Set Home Dir"
fi
