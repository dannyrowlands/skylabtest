if ($http_x_forwarded_proto = "https") { ## Safety net to catch SSL-Offloading
    set $fastcgi_https "on";
}