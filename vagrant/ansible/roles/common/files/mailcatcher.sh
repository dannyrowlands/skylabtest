#!/bin/sh

if [ -f "mail.txt" ]; then
   rm mail.txt
   netstat -tulpn | grep :1080 > "mail.txt"
else
    netstat -tulpn | grep :1080 > "mail.txt"
fi

if grep mailcatcher "mail.txt"; then
   echo "Mailcatcher running"
   rm mail.txt
else
   mailcatcher --http-ip=0.0.0.0
   rm mail.txt
fi
