<?php

/**
 * Created by PhpStorm.
 * User: dannyrowlands
 * Date: 30/06/2016
 * Time: 09:17
 */
interface Fixtures
{
    /**
     * Fixture constructor.
     */
    public function __construct();

    /**
     * @return mixed
     */
    public function getLastTenEntries();

    /**
     *
     */
    public function insertEntry();

    /**
     *
     */
    public function updateEntry();

    /**
     * @return mixed
     */
    public function getId();

    /**
     * @param mixed $id
     */
    public function setId($id);

    /**
     * @return mixed
     */
    public function getTimestamp();

    /**
     * @param mixed $timestamp
     */
    public function setTimestamp($timestamp);

    /**
     * @return mixed
     */
    public function getFeedTime();

    /**
     * @param mixed $feedTime
     */
    public function setFeedTime($feedTime);

    /**
     * @return mixed
     */
    public function getTeams();

    /**
     * @param mixed $teams
     */
    public function setTeams($teams);

    /**
     * @return mixed
     */
    public function getLocation();

    /**
     * @param mixed $location
     */
    public function setLocation($location);

    /**
     * @return mixed
     */
    public function getKickoffDateTime();

    /**
     * @param mixed $kickoffDateTime
     */
    public function setKickoffDateTime($kickoffDateTime);

    /**
     * @return mixed
     */
    public function getResult();

    /**
     * @param mixed $result
     */
    public function setResult($result);

    /**
     * @return mixed
     */
    public function getUpdated();

    /**
     * @param mixed $updated
     */
    public function setUpdated($updated);

    /**
     * @return mixed
     */
    public function getCreated();

    /**
     * @param mixed $created
     */
    public function setCreated($created);
}