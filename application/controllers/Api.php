<?php
/**
 * Created by PhpStorm.
 * User: dannyrowlands
 * Date: 29/06/2016
 * Time: 14:51
 */


/**
 * Class Api
 * @package API
 */
class Api extends CI_Controller
{

    /**
     * @var array
     */
    protected $data;

    /**
     * @var object
     */
    public $load;

    /**
     * @var object
     */
    public $model;

    /**
     *
     */
    public function index()
    {
        $this->load->model('Fixture', '', false);
        $this->load->model('MatchFixture', '', false);
        $this->load->model('ReportFixture', '', false);

        $this->init();
        $this->normalizeData();
    }

    /**
     *
     */
    protected function init()
    {

    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return object
     */
    public function getLoad()
    {
        return $this->load;
    }

    /**
     * @param object $load
     */
    public function setLoad($load)
    {
        $this->load = $load;
    }

    /**
     *
     */
    protected function normalizeData()
    {
        //This needs a better way of identifying which feed is being passed
        if (isset($this->getData()['playerList'])) {
            //Process Match Report Feed
            $this->normalizeMatchData();
        } else {
            //Process Fixtures Feed
            $this->normalizeFixtureData();
        }
    }

    /**
     *
     */
    protected function normalizeFixtureData()
    {
        //Create Fixture Data Model - should be overwritten in subclass
        $this->fixture = new MatchFixture();
    }

    /**
     *
     */
    protected function normalizeMatchData()
    {
        //Create Match Data Model - should be overwritten in subclass
        $this->match = new ReportFixture();
    }
}