#!/bin/env bash

# Simple script to enable or disable the xdebug extension

case $1 in
  on)
    if [ -e /etc/php.d/xdebug.ini.deactivated ]
    then
      sudo mv /etc/php.d/xdebug.ini.deactivated /etc/php.d/xdebug.ini
      sudo service php-fpm restart
      sudo service nginx restart
   else
      echo "Xdebug is already on"
   fi
  ;;
  off)
   if [ -f /etc/php.d/xdebug.ini ]
     then
      sudo mv /etc/php.d/xdebug.ini /etc/php.d/xdebug.ini.deactivated
      sudo service php-fpm restart
      sudo service nginx restart
     else
       echo "Xdebug is already off"
     fi
  ;;
  *)
    echo "Usage: php_debug on|off"
  ;;
esac