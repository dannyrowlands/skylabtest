<?php
require_once ("interfaces/Fixtures.php");

/**
 * Created by PhpStorm.
 * User: dannyrowlands
 * Date: 29/06/2016
 * Time: 23:08
 */
class Fixture extends CI_Model implements Fixtures
{
    /**
     * @var
     */
    public $id;
    /**
     * @var
     */
    public $timestamp;
    /**
     * @var
     */
    public $feedTime;
    /**
     * @var
     */
    public $teams;
    /**
     * @var
     */
    public $location;
    /**
     * @var
     */
    public $kickoffDateTime;
    /**
     * @var
     */
    public $result;
    /**
     * @var
     */
    public $updated;
    /**
     * @var
     */
    public $created;

    /**
     * Fixture constructor.
     */
    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function getLastTenEntries()
    {
        //Retrieve last 10 entries from DB
        $query = $this->db->get('entries', 10);
        return $query->result();
    }

    /**
     *
     */
    public function insertEntry()
    {
        //Insert data into db
        $this->created = time();
        $this->db->insert('entries', $this);
    }

    /**
     *
     */
    public function updateEntry()
    {
        $this->updated = time();
        $this->db->update('entries', $this, $this->id);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * @param mixed $timestamp
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
    }

    /**
     * @return mixed
     */
    public function getFeedTime()
    {
        return $this->feedTime;
    }

    /**
     * @param mixed $feedTime
     */
    public function setFeedTime($feedTime)
    {
        $this->feedTime = $feedTime;
    }

    /**
     * @return mixed
     */
    public function getTeams()
    {
        return $this->teams;
    }

    /**
     * @param mixed $teams
     */
    public function setTeams($teams)
    {
        $this->teams = $teams;
    }

    /**
     * @return mixed
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param mixed $location
     */
    public function setLocation($location)
    {
        $this->location = $location;
    }

    /**
     * @return mixed
     */
    public function getKickoffDateTime()
    {
        return $this->kickoffDateTime;
    }

    /**
     * @param mixed $kickoffDateTime
     */
    public function setKickoffDateTime($kickoffDateTime)
    {
        $this->kickoffDateTime = $kickoffDateTime;
    }

    /**
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param mixed $result
     */
    public function setResult($result)
    {
        $this->result = $result;
    }

    /**
     * @return mixed
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param mixed $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param mixed $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     *
     */
    public function save()
    {

    }
}