<?php
include_once ("Fixture.php");

/**
 * Created by PhpStorm.
 * User: dannyrowlands
 * Date: 29/06/2016
 * Time: 23:08
 */
class ReportFixture extends Fixture
{
    public $playerList;
    public $scorers;
    public $goalTimes;
    public $penalizedPlayers;
    public $penalizedTimes;

    /**
     * @return mixed
     */
    public function getPlayerList()
    {
        return $this->playerList;
    }

    /**
     * @param mixed $playerList
     */
    public function setPlayerList($playerList)
    {
        $this->playerList = $playerList;
    }

    /**
     * @return mixed
     */
    public function getScorers()
    {
        return $this->scorers;
    }

    /**
     * @param mixed $scorers
     */
    public function setScorers($scorers)
    {
        $this->scorers = $scorers;
    }

    /**
     * @return mixed
     */
    public function getGoalTimes()
    {
        return $this->goalTimes;
    }

    /**
     * @param mixed $goalTimes
     */
    public function setGoalTimes($goalTimes)
    {
        $this->goalTimes = $goalTimes;
    }

    /**
     * @return mixed
     */
    public function getPenalizedPlayers()
    {
        return $this->penalizedPlayers;
    }

    /**
     * @param mixed $penalizedPlayers
     */
    public function setPenalizedPlayers($penalizedPlayers)
    {
        $this->penalizedPlayers = $penalizedPlayers;
    }

    /**
     * @return mixed
     */
    public function getPenalizedTimes()
    {
        return $this->penalizedTimes;
    }

    /**
     * @param mixed $penalizedTimes
     */
    public function setPenalizedTimes($penalizedTimes)
    {
        $this->penalizedTimes = $penalizedTimes;
    }
}