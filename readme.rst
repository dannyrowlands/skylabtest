###################
SkyLab Test
###################



*******************
Framework Chosen
*******************

CodeIgniter 3 was chosen because I am familiar with CodeIgniter and it allows me to make use of it's libraries and the power this brings.  Saves me having to re-invent the wheel.

*******************
Methodology
*******************

I have created model classes to contain the structure for both feed types.  These extend a base model that contains common code.  I have also created an interface that both models implement.  The API controller holds the base API code with the Api_Json class extending this and doing the JSON specific data conversion.  (Please note I am aware the Api_Json.php class breaks the CamelCase convention, but it is required for CI to load it without further configuration).


*******************
Files created for test
*******************

The files within the **model**, **interface** and **controller** folders are the files created for this task.



I have run out of time now, so I hope this gives an idea of my thought processes and methodology.