<?php
require_once ("Api.php");

/**
 * Created by PhpStorm.
 * User: dannyrowlands
 * Date: 29/06/2016
 * Time: 19:15
 */
class Api_Json extends Api
{
    /**
     *
     */
    public function init()
    {
        $this->decodeJson();
        $this->outputMethod();
        $this->outputData();
    }

    /**
     *
     */
    public function outputData()
    {
        print_r($this->data);
    }

    /**
     *
     */
    public function outputMethod()
    {
        print $this->input->method()." -- ";
    }

    /**
     *
     */
    protected function normalizeFixtureData()
    {
        //Create Fixture Data Model
        $this->model = new MatchFixture();
        $this->populateCommonValues();
    }

    /**
     *
     */
    protected function normalizeMatchData()
    {
        //Create Match Data Model
        $this->model = new ReportFixture();
        $this->populateCommonValues();
        $this->model->setPlayerList($this->data['playerList']);
        $this->model->setScorers($this->data['scorers']);
        $this->model->setGoalTimes($this->data['goalTimes']);
        $this->model->setPenalizedPlayers($this->data['pplayers']);
        $this->model->setPenalizedTimes($this->data['ptimes']);
    }

    private function decodeJson()
    {
        $this->data = json_decode($this->input->input_stream('data'), true);
        $this->loopForEachElement();
    }

    private function loopForEachElement()
    {
        if (count($this->data) == 0) {
            return;
        }
        foreach ($this->data as $data) {
            $this->setData($data);
            $this->normalizeData();
            $this->model->save();
        }
    }

    protected function populateCommonValues()
    {
        //Populate model
        $this->model->setId(uniqid());
        $this->model->setCreated(time());
        $this->model->setFeedTime($this->data['time']);
        $this->model->setTeams($this->data['teams']);
        $this->model->setLocation($this->data['location']);
        $this->model->setKickoffDateTime($this->data['kodatetime']);
        $this->model->setResult($this->data['result']);
        $this->model->setUpdated(time());
    }
}